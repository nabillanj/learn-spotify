//
//  GridViewCell.swift
//  LearnSpotify
//
//  Created by Nabilla on 13/02/20.
//  Copyright © 2020 PT Intersolusi Teknologi Asia. All rights reserved.
//

import UIKit

class GridViewCell: BaseCell {

    @IBOutlet weak var imgView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func bind(model: ChildrenModel) {
        guard let image = model.image else {return}
        imgView.loadFromUrl(url: image.url)
        imgView.contentMode = .scaleAspectFill
    }
}

extension UIImageView {
    func loadFromUrl(url: String) {
        URLSession.shared.dataTask(with: URL(string: url)!) { (data, response, error) in
            guard let data = data, error == nil else {return}
        
            let image = UIImage(data: data)
            DispatchQueue.main.async {
                self.image = image
            }
        }.resume()
    }
}

