//
//  ListViewCell.swift
//  LearnSpotify
//
//  Created by Nabilla on 13/02/20.
//  Copyright © 2020 PT Intersolusi Teknologi Asia. All rights reserved.
//

import UIKit

class ListViewCell: BaseCell {

    @IBOutlet weak var lblList: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func bind(model: ChildrenModel) {
        lblList.text = model.title
    }

}
