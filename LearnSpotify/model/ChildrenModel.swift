//
//  ChildrenModel.swift
//  LearnSpotify
//
//  Created by Nabilla on 13/02/20.
//  Copyright © 2020 PT Intersolusi Teknologi Asia. All rights reserved.
//

import UIKit
import SwiftyJSON

class ChildrenModel: NSObject {
    var title: String = ""
    var metadatatext: String = ""
    var image: ChildrenImage?
    
    init(withJson json: JSON) {
        self.title = json["title"].stringValue
        self.metadatatext = json["metadatatext"].stringValue
        self.image = ChildrenImage(withJson: json["image"])
    }
}

class ChildrenImage: NSObject {
    var url: String = ""
    var placeholder: String = ""
    
    init(withJson json: JSON) {
        self.url = json["url"].stringValue
        self.placeholder = json["placeholder"].stringValue
    }
}
