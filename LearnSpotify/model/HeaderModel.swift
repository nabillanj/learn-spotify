//
//  HeaderModel.swift
//  LearnSpotify
//
//  Created by Nabilla on 13/02/20.
//  Copyright © 2020 PT Intersolusi Teknologi Asia. All rights reserved.
//

import UIKit
import SwiftyJSON

class HeaderModel: NSObject {
    var id: String = ""
    var component: String = ""
    var header: HeaderTitle?
    
    init(withJson json: JSON) {
        self.id = json["id"].stringValue
        self.header = HeaderTitle(withJson: json["header"])
        self.component = json["component"].stringValue
    }
}

class HeaderTitle: NSObject {
    var title: String = ""
    
    init(withJson json: JSON) {
        self.title = json["title"].stringValue
    }
}
