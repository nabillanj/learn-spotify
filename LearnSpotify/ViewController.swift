//
//  ViewController.swift
//  LearnSpotify
//
//  Created by Nabilla on 13/02/20.
//  Copyright © 2020 PT Intersolusi Teknologi Asia. All rights reserved.
//

import UIKit
import SwiftyJSON

enum ComponentType: String {
    case grid = "grid"
    case list = "list"
}

class ViewController: UIViewController {

    @IBOutlet weak var collectionView: UICollectionView!
    private var baseUrl: String = "https://private-5edef-sampleproject5.apiary-mock.com/api/component/"
    
    private var listOfComponent: [HeaderModel] = []
    private var listOfChildren: [ChildrenModel] = []
    private var componentType: ComponentType = .list
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        collectionView.register(GridViewCell.nib, forCellWithReuseIdentifier: GridViewCell.identifier)
        collectionView.register(ListViewCell.nib, forCellWithReuseIdentifier: ListViewCell.identifier)
        getListData(withUrl: componentType.rawValue)
        setupToolbar()
    }
    
    func setupToolbar() {
        navigationItem.title = "Hello"
        let barItem = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(changeLayoutView))
        navigationItem.rightBarButtonItems = [barItem]
    }
    
    @objc func changeLayoutView() {
        switch componentType {
        case .grid:
            getListData(withUrl: "list")
        case .list:
            getListData(withUrl: "grid")
        default:
            break
        }
    }
    
    func getListData(withUrl urlStr: String) {
        listOfComponent.removeAll()
        listOfChildren.removeAll()

        guard let url = URL(string: baseUrl+urlStr) else {return}
        URLSession.shared.dataTask(with: url) { (data, response, error) in
            guard let data = data, error == nil else { return }
            do {
                let json = try JSON(data: data)
                if let components = json["components"].array {
                    for dataHeader in components {
                        self.listOfComponent.append(HeaderModel(withJson: dataHeader))
                        if let children = dataHeader["children"].array {
                            for dataChildren in children {
                                self.listOfChildren.append(ChildrenModel(withJson: dataChildren))
                            }
                            DispatchQueue.main.async { [weak self] in
                                self?.collectionView.reloadData()
                            }
                        }
                    }
                }
            } catch {
                
            }
        }.resume()
    }
}

extension ViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return listOfChildren.count
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return listOfComponent.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let children = listOfChildren[indexPath.row]
        let componentType = listOfComponent[indexPath.section]
        
        let type = ComponentType(rawValue: componentType.component)
        switch type {
        case .grid?:
            let gridCell = collectionView.dequeueReusableCell(withReuseIdentifier: GridViewCell.identifier, for: indexPath) as! GridViewCell
            gridCell.bind(model: children)
            
            return gridCell
        default:
            let tableCell = collectionView.dequeueReusableCell(withReuseIdentifier: ListViewCell.identifier, for: indexPath) as! ListViewCell
            tableCell.bind(model: children)

            return tableCell
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let component = listOfComponent[indexPath.section]

        let type = ComponentType(rawValue: component.component)
        switch type {
        case .grid?:
            return CGSize(width: self.view.bounds.width / 2 - 16, height: self.view.bounds.height / 4)
        default:
            return CGSize(width: self.view.bounds.width, height: 50)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: 8, bottom: 0, right: 8)
    }
}

